all : clean arp_send

arp_send: myarp.o main.o
	g++ -g -o arp_send main.o myarp.o -lpcap

main.o:
	g++ -std=c++14 -g -c -o main.o main.cpp

myarp.o:
	g++ -std=c++14 -g -c -o myarp.o myarp.cpp

clean:
	rm -rf main.o