#include "myarp.h"

void getmyMAC(u_char * mac, char * interface){
  int mib[6]; size_t len; char *buf;
  mib[0] = CTL_NET;
  mib[1] = AF_ROUTE;
  mib[2] = 0;
  mib[3] = AF_LINK;
  mib[4] = NET_RT_IFLIST;
  if ((mib[5] = if_nametoindex(interface)) == 0) {
      perror("if_nametoindex error");
      exit(2);
  }

  if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
      perror("sysctl 1 error");
      exit(3);
  }

  if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
      perror("sysctl 2 error");
      exit(4);
  }
  struct if_msghdr * ifm = (struct if_msghdr *)buf;
  struct sockaddr_dl * sdl = (struct sockaddr_dl *)(ifm + 1);
  u_char *ptr = (u_char *)LLADDR(sdl);
  memcpy(mac,ptr,MAC_LEN);
}

void strtoip(u_char *dst_addr, char *src_addr){
  uint32_t ia=inet_addr(src_addr);
  memcpy(dst_addr,&ia,IP_LEN);
}

void send_ARP_packet(pcap_t* handle, u_char* ether_dst, u_char* ether_src, u_char *src_MAC, u_char *src_IP, u_char *dst_MAC, u_char *dst_IP, u_short ARP_TYPE){
  ether_header ether;
  memcpy(ether.ether_dhost,ether_dst,MAC_LEN);
  memcpy(ether.ether_shost,ether_src,MAC_LEN);
  ether.ether_type=htons(ETHERTYPE_ARP);
  u_char packet[sizeof(ether_header)+sizeof(ether_arp)];
  ether_arp arp;
  //arp.ea_hdr={htons(ARPHRD_ETHER),htons(ETHERTYPE_IP),MAC_LEN,IP_LEN,htons(ARP_TYPE)};
  
  arp.ea_hdr.ar_hln=MAC_LEN;
  arp.ea_hdr.ar_pln=IP_LEN;
  arp.ea_hdr.ar_hrd=htons(1);
  arp.ea_hdr.ar_pro=htons(ETHERTYPE_IP);
  arp.ea_hdr.ar_op=htons(ARP_TYPE);

  memcpy(arp.arp_sha,src_MAC,MAC_LEN);
  memcpy(arp.arp_tha,dst_MAC,MAC_LEN);
  memcpy(arp.arp_spa,src_IP,IP_LEN);
  memcpy(arp.arp_tpa,dst_IP,IP_LEN);

  memcpy(packet,&ether,sizeof(ether_header));
  memcpy(packet+sizeof(ether_header),&arp,sizeof(ether_arp));

  for(int i=0;i<42;i++){
    printf("%02x ",packet[i]);
    if(i%8==7) printf("\n");
  }

  if(pcap_sendpacket(handle, packet, sizeof(ether_header)+sizeof(ether_arp))==-1){
    printf("Error while sending packet!\n");
    exit(-1);
  }
  printf("Sending... please wait\n\n");
}

void getmyIP(u_char *ip_addr, char *interface){
  struct ifreq ifr;
  int fd = socket(AF_INET, SOCK_DGRAM, 0);
  ifr.ifr_addr.sa_family = AF_INET;
  strncpy(ifr.ifr_name, interface, IFNAMSIZ-1);
  ioctl(fd, SIOCGIFADDR, &ifr);
  close(fd);
  //printf("%s\n", inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
  strtoip(ip_addr,inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr));
}

void printIP(const u_char *addr){
  for(int i=0;i<4;i++){
    printf("%d",*(addr+i));
    if(i!=3) printf(".");
  }
}

void printMAC(const u_char *addr){
  for(int i=0;i<6;i++){
    printf("%02x",*(addr+i));
    if(i!=5) printf(":");
  }
}