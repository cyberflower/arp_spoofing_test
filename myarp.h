#include <pcap.h>
#include <stdio.h>
#include <stdint.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <net/if.h>
#include <netinet/in.h>
#include <sys/ioctl.h>
#include <sys/sysctl.h>
#include <string.h> 
#include <string>     
#include <net/if_dl.h>
#include <sys/socket.h>
#include <netdb.h>
#include <ifaddrs.h>
#include <stdlib.h>
#include <unistd.h>

#pragma comment(lib,"lphlpapi.lib")

#define IP_LEN 4
#define MAC_LEN 6

struct Addr{
  u_char MAC_addr[MAC_LEN];
  u_char IP_addr[IP_LEN];
};

// https://stackoverflow.com/questions/10593736/mac-address-from-interface-on-os-x-c
// get MAC address in OSX(MAC OS)
// This function changes Address structure by call by reference! Be careful! -> by cyberflower's debugging 
void getmyMAC(u_char * mac, char * interface);

// convert string to ip address
void strtoip(u_char *dst_addr, char *src_addr);

// send arp packet to destination -> source
void send_ARP_packet(pcap_t* handle, u_char* ether_dst, u_char* ether_src, u_char *src_MAC, u_char *src_IP, u_char *dst_MAC, u_char *dst_IP, u_short ARP_TYPE);

// http://www.geekpage.jp/en/programming/linux-network/get-ipaddr.php
// get My IP address in en0
void getmyIP(u_char *ip_addr);

// Debugging function...
void printIP(const u_char *addr);
void printMAC(const u_char *addr);